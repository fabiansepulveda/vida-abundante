<?php
/*
Plugin Name: Metabox Plugin
Description: Crea los metabox para el sitio Edredona
Version: 0.1
Author: Dynamik Collective
Author URI: http://www.dynamikcollective.com/
*/

add_action( 'cmb2_init', 'register_metabox_dynamik' );
function register_metabox_dynamik() {
	
	$prefix = '_pmd_';
	
	/*Producto*/
	/*$producto = new_cmb2_box( array(
		'id'            => $prefix . 'product_option',
		'title'         => __( 'Mostrar en:(home)', 'metabox-dynamik' ),
		'object_types'  => array( 'product', ), // Post type
		'context'       => 'side',
		'priority'      => 'default',
		'show_names'    => false, // Show field names on the left
	) );
		$producto->add_field( array(
			//'name' => __( 'Producto destacado en el home', 'metabox-dynamik' ),
			'desc' => 'Nuevo producto',
			'id'   => $prefix . 'new_prod',
			'type'    => 'checkbox'
		) );
		$producto->add_field( array(
			//'name' => __( 'Producto destacado en el home', 'metabox-dynamik' ),
			'desc' => 'Favorito tienda',
			'id'   => $prefix . 'favorite_prod',
			'type'    => 'checkbox'
		) );*/
}

/**** METABOX TAXONOMY  *****/

function cmb2_taxonomy_meta_initiate() {

    //require_once( 'CMB2/init.php' );
    require_once(get_template_directory() . '/lib/CMB2/init.php');
    require_once( 'Taxonomy_MetaData/Taxonomy_MetaData_CMB2.php' );

    /**
     * Semi-standard CMB2 metabox/fields array
     */
    $meta_box = array(
        'id'         => 'cat_options',
        // 'key' and 'value' should be exactly as follows
        'show_on'    => array( 'key' => 'options-page', 'value' => array( 'unknown', ), ),
        'show_names' => true, // Show field names on the left
        //'name' => __( 'Configuración Extra Categorías de productos', 'taxonomy-metadata' ),
        'fields'     => array(
            array(
                'name' => __( 'Revolution Slider Categoría', 'taxonomy-metadata' ),
                'id'   => 'rs_category',
                'type' => 'wysiwyg',
				'options' => array( 'textarea_rows' => 5),
            ),
        )
    );
	

    // (Recommended) Use wp-large-options
    require_once( 'wp-large-options/wp-large-options.php' );
    $overrides = array(
        'get_option'    => 'wlo_get_option',
        'update_option' => 'wlo_update_option',
        'delete_option' => 'wlo_delete_option',
    );

    /**
     * Instantiate our taxonomy meta class
     */
    //$cats 		= new Taxonomy_MetaData_CMB2( 'product_cat', $meta_box, __( 'Revolutions Slider Categoría', 'taxonomy-metadata' ), $overrides );
}
cmb2_taxonomy_meta_initiate();