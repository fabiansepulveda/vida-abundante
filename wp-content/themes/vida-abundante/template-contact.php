<?php /* Template Name: Map Template */ ?>
<?php get_header();?>
<div class="wrapper-main">
	<div class="row">
		<div class="col-xs-12 col-sm-8 map">
			<div id="map-canvas" style="width:100%; height:530px;"></div>
		</div>
		<?php 
		if ( have_posts() ) {
			echo '<div class="col-xs-12 col-sm-4 content-info">';
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			}
			while ( have_posts() ) {
				the_post(); 	
				the_title('<h1>','</h1>');	
				the_content();
			} // end while
			echo '</div>';
		} // end if
		?>
	</div>
</div>
<?php get_footer();?>