<?php get_header();
global $post;
$category = get_the_category($post->ID);
$name_cat = $category[0]->name; // first category name?>
<div class="wrapper-content-info single">
    <div class="container">
        <div class="content-info col-xs-12">

            <?php
            if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
            }
            ?>
            <div class="row">
                <h2><?php echo $name_cat; ?></h2>
            </div>
            <div class="clear"></div>
            <section>
                <?php if ( have_posts() ) {
                    while ( have_posts() ) { 
                        the_post();?>
                        <article class="col-xs-12 col-sm-8">
                            <?php the_title('<h1>','</h1>'); ?>
                            <time><?php the_date();?></time>
                            <div class="resume">
                                <?php the_excerpt(); ?>
                            </div>
                            <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                            <div class="text-box">
                                <?php the_content(); ?>
                            </div>
                        </article>
                        <aside class="col-xs-12 col-sm-4">
                            <h3>Sucede en <strong>Facebook</strong><span><i class="fa fa-facebook" aria-hidden="true"></i></span></h3>
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Figlesiawesleyanavidaabundante&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </aside>
                    <?php } 
                    wp_reset_postdata();
                } ?>
            </section>
        </div>
    </div>
</div>
<?php get_footer();?>