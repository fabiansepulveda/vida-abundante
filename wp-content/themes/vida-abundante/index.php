<?php get_header();?>
    <div class="wrapper-info">
        <div class="slider">
            <?php echo do_shortcode('[rev_slider alias="main_slider"]'); ?>
        </div>
        <div class="clear"></div>
        <div class="wrapper-content">
            <div class="container">
                <div class="col-xs-12 col-sm-8">
                    <div class="module-1">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="box-info">
                                    <h2>¿Quiénes <strong>somos?</strong></h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="text-center">
                                        <a href="">Ver <strong>más...</strong></a>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 box-img">
                                <img src="<?php echo bloginfo('template_url')?>/assets/img/module-img.jpg" />
                            </div>
                        </div>
                    </div><!-- /module-1-->
                    <div class="module-2">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 box-img">
                                <img src="<?php echo bloginfo('template_url')?>/assets/img/module-img.jpg" />
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="box-info">
                                    <h4>Noticias</h4>
                                    <h3>Titulo noticias</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    <div class="text-center">
                                        <a href="">Ver <strong>más...</strong></a>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div><!-- /module-2-->
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="horarios">
                        <h4>Nuestros <strong>Horarios</strong></h4>
                        <ul>
                            <li><strong>Martes:</strong> <p>Ayuno y oración </p><span>9:00 a.m.</span> </li>
                            <li><strong>Jueves:</strong> <p>Servición de adoración y alabanza </p><span>6:30 p.m.</span> </li>
                            <li><strong>Domingo:</strong> <p>Ayuno </p><span>8:00 a.m.</span> </li>
                            <li><strong>Domingo:</strong> <p>Escuela dominical</p><span>9:00 a.m.</span> </li>
                            <li><strong>Domingo:</strong> <p>Servicio de adoración y alabanza</p><span>10:00 a.m.</span> </li>
                        </ul>
                    </div>
                    <div class="map" id="map-canvas">
                    </div>
                </div>
            </div>
        </div>
    </div><!-- wrapper-info-->
<?php get_footer();?>