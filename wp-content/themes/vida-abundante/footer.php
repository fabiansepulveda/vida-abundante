    <footer>
        <div class="container">
            <div class="box-left col-xs-12 col-sm-6">
                
            </div>
            <div class="box-right col-xs-12 col-sm-6">
                <div class="social-net">
                    <ul>
                        <li><a title="Facebook" href="<?php echo theme_get_option( 'facebooklink' ); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a title="Instagram" href="<?php echo theme_get_option( 'youtubelink' ); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                </div>  
            </div>
        </div>
    </footer>
</div><!-- #general-wrapper -->
<?php wp_footer(); ?>
</body>
</html>