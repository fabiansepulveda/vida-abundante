<?php
$search_query = get_search_query();
if($search_query == "")
	$search_query = "";
?>

<form role="search" method="get" id="searchform" class="search-box-form" action="<?php echo home_url( '/' ); ?>">
    <input class="search-imput" placeholder="Buscar..." type="text" value="<?php echo $search_query; ?>" name="s" id="s" onblur="if (this.value == '') {this.value = '';}" onfocus="if (this.value == '') {this.value = '';}" />
    <button type="submit" class="search-submit"><span class="screen-reader-text"><i class="fa fa-search" aria-hidden="true"></i></span></button>
</form>