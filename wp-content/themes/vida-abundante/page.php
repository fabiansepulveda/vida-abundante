<?php get_header();?>
<div class="wrapper-main">
	<?php global $post;
	$children = get_pages( array( 'child_of' => $post->ID ) );
	if ( is_page() && $post->post_parent ) :
	//This is a child-page.
		get_template_part( 'partials/page', 'child' );

	elseif ( is_page() && count( $children ) > 0 ) :
	//This is a parent-page (with one or more children)
		get_template_part( 'partials/page', 'parent' );

	else :
	//This is a parent page without children.
		get_template_part( 'partials/page', 'default' );
	endif; ?>
</div>
<?php get_footer();?>