<?php get_header();
$this_category 	= get_category($cat);
$name_cat 		= $this_category->cat_name;
$term_id_cat	= $this_category->term_id;?>
<div class="wrapper-content-info category">
    <div class="container">
        <div class="content-info col-xs-12">

            <?php
            if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
            }
            ?>
            <div class="row">
                <h1><?php echo $name_cat; ?></h1>
            </div>
            <div class="clear"></div>
            <section>
                <?php if ( have_posts() ) {
                    while ( have_posts() ) { 
                        the_post();?>
                        <article class="col-xs-12 col-sm-6 col-md-4">
                            <div class="item">
                                <div class="img-box">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('full'); ?>
                                    </a>
                                </div>
                                <h2><a href="<?php the_permalink(); ?>">
                                        <?php the_title(); ?>
                                </h2></a>
                                <p><a href="<?php the_permalink(); ?>">
                                    <?php echo substr(get_the_excerpt(), 0,120); ?>...
                                </a></p>
                                <a href="<?php the_permalink(); ?>" class="btn-more"> Leer más </a>
                            </div>
                        </article>
                    <?php } 
                    if ( function_exists('base_pagination') ) { base_pagination(); } else if ( is_paged() ) { ?>
                        <div class="navigation clean-box">
                            <div class="alignleft"><?php next_posts_link('&laquo; Previous Entries') ?></div>
                            <div class="alignright"><?php previous_posts_link('Next Entries &raquo;') ?></div>
                        </div>
                    <?php } 
                    wp_reset_postdata();
                } ?>
            </section>
        </div>
    </div>
</div>
<?php get_footer();?>