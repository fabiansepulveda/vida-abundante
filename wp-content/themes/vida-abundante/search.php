<?php get_header(); ?>
<div id="content">
	<div class="page-result">
		<div class="col-xs-12 col-sm-9 content-info">
			<h1><i class="fa fa-search" aria-hidden="true"></i> Resultados de búsqueda</h1>
			<h4><b>Criterio de búsqueda:</b> <?php echo get_search_query();?></h4>
			<?php if ( have_posts() ) : 
				echo '<div class="find">';
					while ( have_posts() ) : the_post();?>
						<?php wc_get_template_part( 'content', 'product-search' ); ?>
					<?php endwhile; ?>
				</div>
			<?php else : ?>
				<div class="not-find">
					<h3>
						<img src="<?php bloginfo("stylesheet_directory");?>/assets/img/error.png">
						No se encontrarón resultados para su busqueda
					</h3>
						<ul>
							<li>Asegurese que las palabras están bien escritas.</li>
							<li>Intente con diferentes palabras claves.</li>
						</ul>
				</div>
			<?php endif; ?>
		</div>
		<div class="col-xs-12 col-sm-3 sidebar">
			<div class="row">
				<?php $args = array(
					'post_type' => 'product',
					'posts_per_page' => 3,
					'meta_query' => array(
						array(
							'key' => '_pmd_favorite_prod',
							'value' => 'on'
						),
					),
				);

				$query_favorite_product = new WP_Query( $args );
				if ( $query_favorite_product->have_posts() ) { 
					echo "<h2>Productos destacados</h2>";
					while ( $query_favorite_product->have_posts() ) { 
						$query_favorite_product->the_post();
						$product = new WC_Product( get_the_ID() );?>
						<div class="item">
							<a href="<?php the_permalink();?>">
								<?php the_post_thumbnail("shop_catalog");?>
							</a>
							<a class="info-box" href="<?php the_permalink();?>">
								<?php the_title('<h3>','</h3>');?>
								<p class="price">Desde <?php echo $product->get_price_html(); ?></p>
								<span>Ver producto</span>
							</a>
						</div>
					<?php } // end WHILE
					wp_reset_postdata();
				} // end IF
				else { ?>
					<p class="alert"><?php _e( 'Not Found .' ); ?></p>
				<?php } ?>
			</div>
		</div>
	</div><!-- /result-->
</div><!-- /content-->
<?php get_footer(); ?>